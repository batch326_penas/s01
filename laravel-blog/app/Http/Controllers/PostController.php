<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    //Controller is for userlogout
    public function logout(){
        //It will logout the currently logged in user.

        Auth::logout();

        return redirect('/login');

    }

    // action to return view containing a form for a blog post creation
    public function createPost(){
        return view('posts.create');
    }

}

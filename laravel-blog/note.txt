App Folder - contains the models at its root. Models represents our database eneties and have predefined methods for querying the respective tables that they represent.
	Http folder - Controllers subdirectory contains the project's controllers where we defice our application logic

database folder - migrations subdirectory contains the migrations that we will use to define the structures and data types of our database tables

public folder - where assests such as css, js, images etc can be stored and accessed

resources folder - vies subdirectory is the namespae where all views will be lloked for by our application.

routes folder - web.php file is where we define the routes of our application

TO run your laravel application:
php artisan serve

To install Laravel's laravel/ui package via terminal command:
composer require laravel/ui

Then build the authentication scaffolding via the terminal command:
php artisan ui bootstrap --auth

To create a Model:
php artisan make:model name_model -mc

to migrate:
php artisan migrate

